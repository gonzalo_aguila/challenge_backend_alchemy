const { Post } = require("../../db");

const checkUrl = (str) => {
  return str.match(/\.(jpg|gif|png)$/) != null;
};

const postsController = {
  getAllPosts(req, res) {
    Post.findAll()
      .then((posts) => {
        res.send(posts);
      })
      .catch((err) => {
        res.send(err);
      });
  },
  getOnePost(req, res) {
    Post.findOne({ where: { id: req.params.id } })
      .then((post) => {
        if (post) {
          res.send(post);
        } else {
          res.status(404).send("No hay posts con ese ID");
        }
      })
      .catch((err) => {
        console.log(err);
        res.status(500)
      });
  },
  newPost(req, res) {
    const { content, image, category, title, categoryId } = req.body;
    const isImage = checkUrl(image);
    if (image && isImage) {
      Post.create({ content, image, category, title })
        .then((createdPost) => createdPost.setCategory(categoryId))
        .then((completedPost) => res.send(completedPost))
        .catch((err) => {
          res.send(err);
        });
    } else {
      res.send("URL de imagen incorrecta. Por favor, intente nuevamente.");
    }
  },
  async updatePost(req, res) {
    const postToUpdate = await Post.findOne({ where: { id: req.params.id } });
    if (!postToUpdate) {
      res.send("No hay posteos con ese ID");
    } else {
      Post.update(req.body, {
        where: { id: req.params.id },
      })
        .then(() => res.send("Post actualizado correctamente"))
        .catch((err) => console.log("ERROR " + err));
    }
  },
  deletePost(req, res) {
    Post.destroy({ where: { id: Number(req.params.id) } })
      .then(() => res.send("Post borrado correctamente"))
      .catch((e) => console.log(e));
  },
};

module.exports = postsController;
