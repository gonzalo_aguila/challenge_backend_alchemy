const express = require("express")
const router = express.Router()
const {getAllCategories, getOneCategory, newCategory} = require("./controllers/categoriesController")

router.get("/", getAllCategories)
router.get("/:id", getOneCategory)
router.post("/", newCategory)

module.exports = router