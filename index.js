const express = require("express")
const app = express()
const PORT = 5000
const api = require("./routes")
require("./db")


app.use(express.json())
app.use(express.urlencoded({extended: true}))
app.use("/api", api)


app.listen(PORT, () => {
    console.log(`Server listening in port ${PORT}`)
})