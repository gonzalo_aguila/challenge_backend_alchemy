module.exports = (sequelize, type) => {
    return sequelize.define("post", {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        title: {
            type: type.STRING(50),
            allowNull: false
        },
        content: {
            type: type.STRING(200),
            allowNull: false
        },
        image: {
            type: type.STRING,
        },
    })
}