const express = require("express")
const router = express.Router()
const postsRouter = require("./posts")
const categoriesRouter = require("./categories")

router.use("/posts", postsRouter)
router.use("/categories", categoriesRouter)

 
module.exports = router