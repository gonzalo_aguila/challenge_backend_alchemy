const { Category } = require("../../db");

const categoriesController = {
  getAllCategories(req, res) {
    Category.findAll()
      .then((categories) => {
        res.send(categories);
      })
      .catch((err) => {
        res.send(err);
      });
  },
  getOneCategory(req, res) {
    Category.findOne({ where: { id: req.params.id } })
      .then((category) => {
        if (category) {
          res.send(category);
        } else {
          res.send("No hay categoria con ese ID");
        }
      })
      .catch((err) => {
        console.log(err);
      });
  },
  newCategory(req, res) {
    const { name } = req.body;
    Category.create({ name })
      .then((createdCategory) => res.send(createdCategory))
      .catch((err) => {
        res.send(err);
      });
  },
};

module.exports = categoriesController;
