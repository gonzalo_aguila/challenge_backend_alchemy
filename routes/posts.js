const express = require("express")
const router = express.Router()
const { getAllPosts, 
getOnePost,
newPost,
updatePost,
deletePost} = require("./controllers/postsControllers")

router.get("/", getAllPosts)
router.get("/:id", getOnePost)
router.post("/", newPost)
router.patch("/:id", updatePost)
router.delete("/:id", deletePost)

module.exports = router